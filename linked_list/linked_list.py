# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.


class LinkedList:
  def __init__(self):
    self.head=None
    self.tail=None
    self.length=0
    pass

  def insert(self,value,idx=None):

    if self.length == 0:
      new_node = LinkedListNode(value)
      self.head=new_node
      self.tail=new_node
      self.length += 1

    else:
      if idx is None:
        idx = self.length
        new_node = LinkedListNode(value)
        self.tail.link=new_node
        self.tail=new_node
        self.length += 1
      else:
        previous_node = self.traverse(idx - 1)
        new_node = LinkedListNode(value,previous_node.link)
        previous_node.link = new_node
        self.length += 1

  def traverse(self,idx):
    current=self.head
    while idx > 0:
      current=current.link
      idx -= 1
    return current


  def get(self,idx):
    if idx >= self.length:
      raise IndexError
    if idx < 0:
      idx = self.length + idx
    return self.traverse(idx).value

  def remove(self,idx):
    if idx >= self.length:
      raise IndexError

    if idx == 0:
      removed = self.head
      self.head = removed.link
      self.length -= 1
      return removed.value

    elif idx == self.length - 1:
      removed = self.tail
      self.length -= 1
      self.tail = self.traverse(self.length - 1)
      return removed.value

    else:
      previous_node = self.traverse(idx-1)
      removed = previous_node.link
      previous_node.link = removed.link
      self.length -= 1
      return removed.value


class LinkedListNode:
  def __init__(self,value,link=None):
    self.value = value
    self.link = link
