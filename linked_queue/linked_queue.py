# Write your code here to make the tests pass.
class LinkedQueue:
  def __init__(self):
    self.head = None
    self.tail = None
    self.length = 0


  def dequeue(self):
    removed = self.head
    self.head = removed.link
    self.length -= 1
    if self.length == 0:
      self.tail = None
    return removed.value

  def enqueue(self,value):
    new_node = LinkedQueueNode(value)
    if self.length == 0:
      self.head = new_node
      self.tail = new_node
    else:
      self.tail.link = new_node
      self.tail = new_node
    self.length += 1


class LinkedQueueNode:
  def __init__(self,value,link=None):
    self.value = value
    self.link = link
