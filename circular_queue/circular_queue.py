# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# circular_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_XX.py.



class CircularQueue:
  def __init__(self,size):
    self.size = size
    self.buffer = [None]*size
    self.length = 0
    self.head = 0
    self.tail = 0



  def enqueue(self,value):
    if self.length < self.size:
      self.buffer[self.tail] = value
      self.tail = (self.tail + 1) % self.size
      self.length += 1

  def dequeue(self):
    if self.length > 0:
      removed = self.buffer[self.head]
      self.head = (self.head + 1) % self.size
      self.length -= 1
      return removed
    else:
      raise IndexError
